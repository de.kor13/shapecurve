
let numSegments = 5;
let i_mass=0;
let start_i=0;

const xStart = 0; 
const yStart = 250; 
const diff = 10;
let startPath=5;
let turn=0;
let linesX=[20,10,90,66,77,89,123,180,230,301,350,390,410,450]
let linesY=[20,5,55,166,77,189,223,380,30,31,350,30,410,450]


let xCor = [];
let yCor = [];


function setup() {


  createCanvas(500, 500);
  frameRate(5);
  stroke(255);
  strokeWeight(5);


  for (let i = 0; i < numSegments; i++) {
    xCor.push(linesX[i]);
    yCor.push(linesY[i]);
  }
}

function draw() {
  background(0);
  noFill();

  for (let i = 0; i < numSegments - 1; i++) {
    beginShape();
    curveVertex(xCor[i], yCor[i])
    curveVertex(xCor[i], yCor[i])
    curveVertex(xCor[i + 1], yCor[i + 1])
    curveVertex(xCor[i + 2], yCor[i + 2])
    curveVertex(xCor[i + 3], yCor[i + 3])
    curveVertex(xCor[i + 4], yCor[i + 4])
    curveVertex(xCor[i + 4], yCor[i + 4])
    endShape();

  }
  updateSnakeCoordinates();
  edges();

}



function updateCoordinates() {
 
  let del=start_i;
  let i = 0;
  for (i_mass=start_i; i_mass < del+numSegments ; i_mass++) {
    xCor[i] = linesX[i_mass + 1];
    yCor[i] = linesY[i_mass + 1];
    i++;
  }
  start_i++;    
  
}

function edges(){
   if(xCor[xCor.length - 1]==linesX[linesX.length - 1]){
    start_i=0;
    xCor=[];
    yCor=[];
    for (let i = 0; i < numSegments; i++) {
      xCor.push(linesX[i]);
      yCor.push(linesY[i]);
    }
  }


}

